import scrapy


class Speech(scrapy.Item):
    title = scrapy.Field()
    date = scrapy.Field()
    body = scrapy.Field()


class Spider(scrapy.Spider):

    name = 'parser'

    def __init__(self, candidate=None, type=None, *args, **kwargs):
        super(Spider, self).__init__()
        id = None
        if candidate == "clinton":
            id = 70
        elif candidate == "trump":
            id = 45
        if type == "speech":
            type = 0
        if type == "statement":
            type = 1
        if type == "press":
            type = 2

        self.start_urls = [
            'http://www.presidency.ucsb.edu/2016_election_speeches.php?candidate={0}&campaign=2016{1}&doctype=500{2}'.format(id, candidate.upper(), type)
        ]

    allowed_domains = ['presidency.ucsb.edu']

    def parse(self, response):
        sel = scrapy.Selector(response)
        urls = sel.xpath('//a[contains(@href, "pid")]/@href').extract()
        print(urls)
        for url in urls:
            url = "http://www.presidency.ucsb.edu" + url[2:]
            self.log('Found speech url: %s' % url)
            yield scrapy.Request(url, callback = self.parseSpeech)

    def parseSpeech(self, response):
        sel = scrapy.Selector(response)
        item = Speech()
        item["title"] = sel.xpath('//span[@class="paperstitle"]/text()').extract()
        item["date"] = sel.xpath('//span[@class="docdate"]/text()').extract()
        item["body"] = sel.xpath('//*[@class="displaytext"]/*/text()').extract()
        return item
