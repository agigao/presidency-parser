# Elections 2016 - Data
Built with Scrapy Library
## How to use:
- I assume you have python installed
- scrapy library
`pip install scrapy`
- How to build command:
<br>
`scrapy crawl parser -a candidate={trump or clinton} -a type={speech, statement or press} -o {name of the file}.{desired format of data: json, jsonlines, xml, csv}`
- Example:
`scrapy crawl parser -a candidate=trump -a type=speech -o trump-speech.jsonlines`

- dumped data: already dumped data can be found in .jsonfiles format 
<br>
- Structure(fields) of data:
- - `title` 
- - `date`
- - `body`

<br>
© Giga